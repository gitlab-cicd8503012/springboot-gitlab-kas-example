package com.maryanto.dimas.example.springbootwebgitopsexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringbootGitops {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootGitops.class, args);
    }

    @GetMapping("/")
    public String getIndex() {
        return "It's Works";
    }

}
